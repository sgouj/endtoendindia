package com.allstate.dao;

import com.allstate.entities.Employee;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class EmployeeDaoITTest {

    @Autowired
    private EmployeeData dao;

    @Autowired
    MongoTemplate tpl;

    @Test
    public void save_And_findById_Success() {
        Employee employee = new Employee(144,"Dee", 50,"d@i.ie", 1234.00);

        dao.save(employee);

        assertEquals(employee.getId(), dao.find(144).getId());


    }

    @AfterAll
    public void Cleanup() {
       Employee employee= dao.find(144);
        tpl.remove(employee);

    }
    }


