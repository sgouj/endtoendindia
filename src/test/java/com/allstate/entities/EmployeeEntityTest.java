package com.allstate.entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class EmployeeEntityTest {

    private Employee employee;

    @BeforeEach
    void setUp() {

        employee = new Employee(1,"Dee", 50,"d@i.ie", 1234.00);
    }

    @Test
    void getId() {
        assertEquals(1, employee.getId());
    }


    @Test
    void getName() {
        assertEquals("Dee", employee.getName());
    }

    @Test
    void getSalary() {
        assertEquals(150.00, employee.getSalary());
    }

    @Test
    void getEmail() { assertEquals(1234, employee.getEmail());
    }







}
