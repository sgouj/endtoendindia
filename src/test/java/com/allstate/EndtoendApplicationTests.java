package com.allstate;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class EndtoendApplicationTests {

	@Test
	void contextLoads() {
	}



	@Autowired
	private TestRestTemplate restTemplate;

	int port=8080;
	@Test
	public void greetingShouldReturnDefaultMessage() throws Exception {
		assertThat(this.restTemplate.getForObject("http://localhost:" + port + "/api/status",
				String.class)).contains("Hello, World");
	}
}
