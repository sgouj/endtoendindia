package com.allstate.service;

import java.util.List;

import com.allstate.entities.Employee;

public interface EmployeeService {
    long Total();
    List <Employee> all();
    void save(Employee employee);
    Employee findById(int id);
    long update(Employee employee);
}
