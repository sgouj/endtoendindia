package com.allstate.service;

import java.util.List;

import com.allstate.dao.EmployeeData;
import com.allstate.entities.Employee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeServiceImpl implements EmployeeService {

@Autowired 
private EmployeeData dao;

    @Override
    public long Total() {      
        return dao.count();
    }

    @Override
    public List<Employee> all() {  
        return dao.findall();
    }

    @Override
    public void save(Employee employee) {
       dao.save(employee);
    }

    @Override
    public Employee findById(int id) {      
        return dao.find(id);
    }

    @Override
    public long update(Employee employee) {
        return dao.update(employee);
    }


}
