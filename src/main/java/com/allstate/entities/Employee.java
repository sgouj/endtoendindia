package com.allstate.entities;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Employee extends Person {
    private String email;
    private double salary;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public Employee(String email, double salary) {
        this.email = email;
        this.salary = salary;
    }

    public Employee(int id, String email, double salary) {
        super(id);
        this.email = email;
        this.salary = salary;
    }

    public Employee(int id, String name, int age, String email, double salary) {
        super(id, name, age);
        this.email = email;
        this.salary = salary;
        
        
    }

    public Employee(){}
    
@Override
    public  void display()
    {
            System.out.printf("Values are %s,  %d, %s, %f" , this.getName(), this.getId(), this.email, this.salary);
    }


    public static void printf()
    {
        System.out.printf("EMPLOYEE: Formatted Printed Values");
    }

}
